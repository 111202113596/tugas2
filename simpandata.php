<!DOCTYPE html>
<html lang="en">
<head>
    <title>POST</title>
</head>
<body style="background-color:#eaeaea;">
<form>
    <h1>Selamat Datang, <?php echo $_POST["nama"]; ?>!</h1>
        <table border="1" cellspacing="0" cellpadding="10">
            <thead>
                <th>Nomor</th>
                <td colspan="2">Info Data Pribadi</td>
            </thead>
            <tbody>
                <th>1</th>
                <td>Nama </td>
                <td><?php echo $_POST["nama"]; ?></td>
            </tbody>
            <tbody>
                <th>2</th>
                <td>Alamat </td>
                <td><?php echo $_POST["alamat"]; ?></td>
            </tbody>
            <tbody>
                <th>3</th>
                <td>Jenis Kelamin</td>
                <td><?php echo $_POST['jk']; ?></td>
            </tbody>
            <tbody>
                <th>4</th>
                <td>Hobi</td>
                <td><?php echo join(", ",$_POST['hobi']); ?></td>
            </tbody>
            <tbody>
                <th>5</th>
                <td>Perkerjaan</td>
                <td><?php echo $_POST["perkerjaan"]; ?></td>
            </tbody>           
    </form>
</body>
</html>