<?php
    //no.1
    echo "Jawaban no.1";
    echo "<br>";
    $negaraAsia=["Indonesia"=>"jakarta","India"=>"New Delhi" 
    ,"Jepang"=>"Tokyo","Cina"=>"Beijing"
    ,"Malaysia"=>"Kuala Lumpur","Filipina"=>"Manila"
    ,"Korea Utara"=>"Pyongyang","Korea Selatan"=>"Seoul"
    ,"Iran"=>"Teheran","Irak"=>"Bahgdad","Vietnam"=>"Hanoi"
    ,"Thailand"=>"Bangkok"
    ];
      $x=0;
      foreach($negaraAsia as $key => $value){
      $x++;
      echo "$x. $key ibukotanya $value <br>";
      }
      echo "<br>";

   //2
   echo "Jawaban no.2";
   echo "<br>";
   $suhuUdara = [29,35,38,31,34,36,39,33,34,40,35,32,37,34,34,36,33,36,30,33,41];
   //average
   $jumlah = array_sum($suhuUdara);
   $rata = $jumlah / count($suhuUdara);
   echo "A) Rata-rata suhu adalah " . round($rata,2);
   echo "<br>";
   //Top 5 terendah
   sort($suhuUdara);
   $top5l = array_slice($suhuUdara, 0, 5);
   echo "B) 5 suhu paling rendah adalah ";
   for($i=0;$i<count($top5l);$i++){
      echo ",$top5l[$i]";
   }
   echo "<br>";
   //Top 5 tertinggi
   rsort($suhuUdara);
   $top5 = array_slice($suhuUdara, 0, 5);
   echo "C) 5 suhu paling tinggi adalah ";
   for($i=4;$i>=0;$i--){
      echo ",$top5[$i]";
   }
   echo "<br>";
   echo "<br>";

   //3
   echo "Jawaban no.3";
   echo "<br>";
   $rate =['usd'=>14367,'jpy'=>1192,'cny'=>2262,
         'krw'=>11.87,'myr'=>3416,'sgd'=>10621,
         'gbp'=>19074,'eur'=>15891];
   $x=9;
   foreach($rate as $key => $value){
   $x--;
   echo "$x $key dikonversi menjadi Rp " . $value*$x ;
   echo ",<br>";
   }
   echo "<br><br>";
?>